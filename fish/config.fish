# ENV
if type -q vim
    set -x EDITOR vim
else
    set -x EDITOR vi
end
set -x VISUAL $EDITOR

# PATH
if type -q ruby
    if type -q gem
        fish_add_path (ruby -e 'puts Gem.user_dir')/bin
    end

    if type -q rbenv
    and not string match -qr .rbenv $PATH
        source (rbenv init - | psub)
    end
end

if [ -d $HOME/.cargo ]
    fish_add_path $HOME/.cargo/bin
end

if [ -d $HOME/.local/go/bin ]
    fish_add_path $HOME/.local/go/bin
end

fish_add_path $HOME/.local/bin
fish_add_path $HOME/bin

if status is-interactive
    # Prompt
    switch $hostname
    case 'valhalla*'
        set chost cyan
        set cpath green
        set cgit yellow
    case avalon
        set chost red
        set cpath magenta
        set cgit blue
    case thule
        set chost magenta
        set cpath blue
        set cgit cyan
    case naraka
        set chost green
        set cpath yellow
        set cgit red
    case atlantis
        set chost yellow
        set cpath blue
        set cgit magenta
    case '*'
        set chost green
        set cpath cyan
        set cgit yellow
    end

    function git_ps1 -a head tail
        if type -q git; and set -l git_branch (git symbolic-ref HEAD 2>/dev/null)
            echo -ns \
                $head \
                (set_color $cgit) (basename $git_branch) (set_color normal) \
                $tail
        end
    end

    function fish_prompt
        if fish_is_root_user
            set -f prompt_sym '#'
        else
            set -f prompt_sym '$'
        end

        if [ -n $COLUMNS ]; and [ $COLUMNS -lt 80 ]
            # Short PS1
            echo -ns \
                '[' (set_color $chost) $USER@$hostname (set_color normal) \
                '|' (set_color $cpath) (prompt_pwd) (set_color normal) \
                (git_ps1 '|') \
                ']' $prompt_sym ' '
        else
            # Long PS1
            echo -ns \
                '(' (date +%H:%M) \
                ')' (set_color $chost) $USER@$hostname (set_color normal) \
                ':' (set_color $cpath) (prompt_pwd --full-length-dirs=3) (set_color normal) \
                (git_ps1 '[' ']') \
                $prompt_sym ' '
        end
    end
end
