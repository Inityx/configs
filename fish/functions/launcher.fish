function launcher --argument name
find /usr/share/applications ~/.local/share/applications -iname "*$name*.desktop"
end
