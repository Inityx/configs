function ls --wraps eza
    eza -lhm --time-style=iso $argv
end
