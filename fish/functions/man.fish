function man
    LESS_TERMCAP_mb=(string unescape '\e[01;31m') \
    LESS_TERMCAP_md=(string unescape '\e[01;34m') \
    LESS_TERMCAP_me=(string unescape '\e[0m') \
    LESS_TERMCAP_se=(string unescape '\e[0m') \
    LESS_TERMCAP_so=(string unescape '\e[31m') \
    LESS_TERMCAP_ue=(string unescape '\e[0m') \
    LESS_TERMCAP_us=(string unescape '\e[04;32m') \
    command man $argv
end
