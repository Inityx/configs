function reset
    # normal reset contains an unnecessary delay for hardware terminals
    echo -n \e]\e\\\ec
end
