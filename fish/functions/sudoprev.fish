function sudoprev
    set -l prev (history -Rn1)

    switch (count $prev)
    case 1
        eval sudo $prev
    case 0
        echo 'No previous command'
    case '*'
        echo 'Previous command was multiple lines'
        return 1
    end
end
