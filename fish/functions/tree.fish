function tree --wraps exa
    eza -lhmT --time-style=iso $argv
end
