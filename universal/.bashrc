#!/bin/bash

# Unused variables:
# shellcheck disable=SC2034

# Sourcing files:
# shellcheck disable=SC1091

# ANSI color codes
RS="\e[0m"
# Foreground
FBLK="\e[30m"
FRED="\e[31m"
FGRN="\e[32m"
FYEL="\e[33m"
FBLE="\e[34m"
FMAG="\e[35m"
FCYN="\e[36m"
FWHT="\e[97m"
# Background
BBLK="\e[40m"
BRED="\e[41m"
BGRN="\e[42m"
BYEL="\e[43m"
BBLE="\e[44m"
BMAG="\e[45m"
BCYN="\e[46m"
BWHT="\e[107m"

export PATH

export EDITOR
export VISUAL

export HISTFILESIZE
export HISTTIMEFORMAT

_not_path_has() {
    [[ ! "$PATH" =~ ^(.*:)?"$1"(:.*)?$ ]]
    return $?
}

# ENV
if [ -r "$HOME/.bash_aliases" ]; then
    source "$HOME/.bash_aliases"
fi

if [ -f /usr/share/bash-completion/bash_completion ]; then
    source /usr/share/bash-completion/bash_completion
else
    echo 'Error: bash completion not loaded'
fi

if which vim &>/dev/null; then
    EDITOR="vim"
else
    EDITOR="vi"
fi
VISUAL=$EDITOR

# PATH
if which ruby &>/dev/null; then
    if _not_path_has .gem && which gem &>/dev/null; then
        PATH="$(ruby -e 'puts Gem.user_dir')/bin:$PATH"
    fi

    if _not_path_has .rbenv && which rbenv &>/dev/null; then
        RBENV_ROOT="$HOME/.rbenv" eval "$(rbenv init -)"
    fi
fi

if [ -d "$HOME/.cargo" ]; then
    if _not_path_has .cargo/bin; then
        PATH="$HOME/.cargo/bin:$PATH"
    fi
fi

if _not_path_has "$HOME/bin"; then
    PATH="$HOME/bin:$HOME/.local/bin:$PATH"
fi

# If not running interactively, skip the rest
if [[ ! "$-" =~ i ]]; then
    return
fi

# Prompt
case $HOSTNAME in
valhalla*)
    chost=$FCYN
    cpath=$FGRN
    ;;
avalon)
    chost=$FRED
    cpath=$FMAG
    ;;
naraka)
    chost=$FGRN
    cpath=$FCYN
    ;;
thule)
    chost=$FMAG
    cpath=$FBLE
    ;;
*)
    chost=$FBLE
    cpath=$FCYN
    ;;
esac

truncate() {
    if [ ${#1} -le "$2" ]; then
        printf "%s" "$1"
    else
        printf "…%s" "$(tail -c"$2" <<< "$1")"
    fi
}

git_ps1() {
    if ! which git &>/dev/null; then return; fi

    local git_branch
    local git_prompt

    git_branch=$(git symbolic-ref HEAD 2>/dev/null)
    # shellcheck disable=SC2181
    if [ "$?" -eq 0 ]; then
        git_prompt=$(truncate "$(basename "$git_branch")" "$1");
        printf "%s" "$2" "\\[$FYEL\\]" "$git_prompt" "\\[$RS\\]" "$3"
    fi
}

pwd_ps1() {
    local newPWD

    # If length argument passed, truncate single directory
    if [ -n "$1" ]; then
        # Alias homedir to '~'
        if [ "$HOME" == "$PWD" ]; then
            newPWD='~'
        else
            newPWD=$(truncate "$(basename "$PWD")" "$1")
        fi
    else
        local max_width
        local field_count

        # Collapse home-prefixed paths to '~/.*'
        if [ "$HOME" == "${PWD:0:${#HOME}}" ]; then
            newPWD="~${PWD:${#HOME}}"
        else
            newPWD=$PWD
        fi

        # Truncate to min single directory, max 1/3 width
        max_width=$((COLUMNS / 3))
        field_count=$(tr -cd '/' <<< "$newPWD" | wc -c)

        while [ "${#newPWD}" -gt $max_width ] && [ "$field_count" -gt 1 ]; do
            newPWD=${newPWD#*/}
            ((field_count--))
        done
    fi

    printf "%s" "\\[$cpath\\]" "$newPWD" "\\[$RS\\]"
}

prompt_command() {
    local newPWD
    local git_branch

    if [ -n "$COLUMNS" ] && [ "$COLUMNS" -lt 100 ]; then
        # Short PS1
        newPWD=$(pwd_ps1 12)
        git_branch=$(git_ps1 8 \|)
        PS1="[\\[$chost\\]\\u\\[$RS\\]|${newPWD}${git_branch}]\\$ "
    else
        # Long PS1
        newPWD=$(pwd_ps1)
        git_branch=$(git_ps1 16 [ ])
        PS1="(\\A)\\[$chost\\]\\u@\\h\\[$RS\\]:${newPWD}${git_branch}\\$ "
    fi
}

# Startup
PROMPT_COMMAND=prompt_command
