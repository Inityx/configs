" Base options
filetype on
syntax enable
set number
set lazyredraw
set guicursor=
set showmatch hlsearch
set mouse=a
set backspace=2 tabstop=4 softtabstop=4 shiftwidth=4 expandtab
colorscheme desert

" Vim requires POSIX shell
if &shell =~# 'fish$'
    set shell=sh
endif

" Enable local vimrc
set exrc
set secure

" Conditional options
au FileType sh colorscheme delek
au FileType yaml colorscheme delek
au FileType python colorscheme delek
au FileType ruby colorscheme delek | set tabstop=2 softtabstop=2 shiftwidth=2
au FileType javascript set tabstop=2 softtabstop=2 shiftwidth=2
au FileType typescript set tabstop=2 softtabstop=2 shiftwidth=2

" Shortcuts and autocorrect
cnoreabbrev Q q
cnoreabbrev W w
cnoreabbrev Wq wq
