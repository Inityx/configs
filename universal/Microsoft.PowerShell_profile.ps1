## Powershell PS1 based on the example configuration at
## https://learn.microsoft.com/en-us/powershell/scripting/learn/shell/creating-profiles?view=powershell-7.4

Write-Output "Loading profile $PROFILE"

## Map User+Root registry to virtual drives
if (!(Test-Path HKCR:)) {
    $null = New-PSDrive -Name HKCR -PSProvider Registry -Root HKEY_CLASSES_ROOT
    $null = New-PSDrive -Name HKU -PSProvider Registry -Root HKEY_USERS
}

## Customize the prompt
function prompt {
    $identity = [Security.Principal.WindowsIdentity]::GetCurrent()
    $principal = [Security.Principal.WindowsPrincipal] $identity
    $adminRole = [Security.Principal.WindowsBuiltInRole]::Administrator

    $prefix = $(if (Test-Path variable:/PSDebugContext) { '[DBG]: ' }
                elseif ($principal.IsInRole($adminRole)) { "[ADMIN]: " }
                else { '' })
    $body = 'PS ' + $(Get-Location)
    $suffix = $(if ($NestedPromptLevel -ge 1) { '>>' }) + '> '
    $prefix + $body + $suffix
}

## Create $PSStyle if running on a version older than 7.2
## - Add other ANSI color definitions as needed
if ($PSVersionTable.PSVersion.ToString() -lt '7.2.0') {
    # define escape char since "`e" may not be supported
    $esc = [char]0x1b
    $PSStyle = [pscustomobject]@{
        Foreground = @{
            Magenta = "${esc}[35m"
            BrightYellow = "${esc}[93m"
        }
        Background = @{
            BrightBlack = "${esc}[100m"
        }
    }
}

## Set PSReadLine options and keybindings
$PSROptions = @{
    ContinuationPrompt = '  '
    Colors             = @{
        Operator         = $PSStyle.Foreground.Magenta
        Parameter        = $PSStyle.Foreground.Magenta
        Selection        = $PSStyle.Background.BrightBlack
        InLinePrediction = $PSStyle.Foreground.BrightYellow + $PSStyle.Background.BrightBlack
    }
}
Set-PSReadLineOption @PSROptions
Set-PSReadLineKeyHandler -Chord 'Ctrl+d' -Function ViExit
Set-PSReadLineKeyHandler -Chord 'Enter' -Function ValidateAndAcceptLine

## Add completions for the dotnet CLI tool
$scriptblock = {
    param($wordToComplete, $commandAst, $cursorPosition)
    dotnet complete --position $cursorPosition $commandAst.ToString() |
        ForEach-Object {
            [System.Management.Automation.CompletionResult]::new($_, $_, 'ParameterValue', $_)
        }
}
Register-ArgumentCompleter -Native -CommandName dotnet -ScriptBlock $scriptblock

## Define command aliases
## https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_aliases?view=powershell-7.4#alternate-names-for-commands-with-parameters
function ExaDefault {eza -lhm --time-style=iso $args}
Set-Alias -Name ls -Value ExaDefault

Set-Alias -Name cat      -Value "C:\Program Files\Git\usr\bin\cat.exe"
Set-Alias -Name dos2unix -Value "C:\Program Files\Git\usr\bin\dos2unix.exe"
Set-Alias -Name grep     -Value "C:\Program Files\Git\usr\bin\grep.exe"
Set-Alias -Name less     -Value "C:\Program Files\Git\usr\bin\less.exe"
Set-Alias -Name md5sum   -Value "C:\Program Files\Git\usr\bin\md5sum.exe"
Set-Alias -Name touch    -Value "C:\Program Files\Git\usr\bin\touch.exe"
Set-Alias -Name vim      -Value "C:\Program Files\Git\usr\bin\vim.exe"
Set-Alias -Name which    -Value "C:\Program Files\Git\usr\bin\which.exe"
